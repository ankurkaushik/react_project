import React, { Component } from 'react';
import { Col, Button, FormGroup, Label, Input } from 'reactstrap';
import { signup } from '../service/basicApi';

export default class signUp extends Component {
    constructor(props){
         super(props)
         this.state ={
            first_name:'',
            last_name : '',
            email :'',
            username: '',
            password: ''
        
         }
    }

    onChange = event =>{
        this.setState({
            [event.target.name]: event.target.value
        })
     }

     submit = event =>{
        event.preventDefault();
         console.log("this is submit",this.state)
         signup(this.state,(response) =>{
              console.log("response",response)   
         },(error) =>{
              console.log("this is error",error); 
         })
     }

    render() {
        const {first_name,last_name,email,username,password} = this.state
        return (
            <div>
            <div style={{marginTop: 10}}>
              <h3>signUp</h3>
                  <form onSubmit ={this.submit}>
                     <FormGroup row>
                     <Label sm={2}>First Name</Label>
                     <Col sm={8}>
                       <Input type="text" name="first_name" value={first_name}  onChange={this.onChange}  id="first_name" placeholder="First Name" />
                     </Col>
                      </FormGroup>
                      <FormGroup row>
                     <Label  sm={2}>Last Name</Label>
                     <Col sm={8}>
                       <Input type="text" name="last_name" value={last_name} onChange={this.onChange}  placeholder="Last Name" />
                     </Col>
                      </FormGroup>
                      <FormGroup row>
                     <Label  sm={2}>userName</Label>
                     <Col sm={8}>
                       <Input type="text" name="username"  value={username}  onChange={this.onChange} placeholder="userName" />
                     </Col>
                      </FormGroup>

                      <FormGroup row>
                     <Label  sm={2}>Email</Label>
                     <Col sm={8}>
                       <Input type="email" name="email"  value={email}  onChange={this.onChange} placeholder="Email Id" />
                     </Col>
                      </FormGroup>


                      <FormGroup row>
                     <Label for="exampleEmail" sm={2}>Password</Label>
                     <Col sm={8}>
                       <Input type="password" name="password" value={password} onChange={this.onChange}  placeholder="Password" />
                     </Col>
                      </FormGroup>
                        <FormGroup  row>
                            <Col sm={{ size: 10, offset: 2 }}>
                                <Button type="submit">Submit</Button>
                            </Col>
                        </FormGroup>
                  </form>

              </div>
            </div>
        )
    }
}