
import React, { Component } from 'react';

export default class Create extends Component {
     constructor(props){
           super(props);
           this.state = {
             person:'',
             business:'',
             gstNo:''
        };      
     }     

    

     onChange = event =>{
            this.setState({
                [event.target.name]: event.target.value
            })
            console.log("this is on change",this.state.person)
     }


     submit = event =>{
        event.preventDefault();
         console.log("this is submit",this.state.person)
     }
    render() {
          const {person,business,gstNo} = this.state;
        return (
            
            <div style={{marginTop: 10}}>
            <h3>Add New Business</h3>
            <form onSubmit={this.submit}>
                <div className="form-group">
                    <label>Add Person Name:  </label>
                    <input type="text" className="form-control" name="person" value={person} id="person" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label>Add Business Name: </label>
                    <input type="text" className="form-control" name="business" value={business} id="business" onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <label>Add GST Number: </label>
                    <input type="text" className="form-control" name="gstNo" value={gstNo} id ="gstNo"onChange={this.onChange}/>
                </div>
                <div className="form-group">
                    <input type="submit" className="btn btn-primary"/>
                </div>
            </form>
        </div>
        )

    }
}