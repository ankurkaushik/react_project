import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Header extends Component {
    constructor(props){
         super(props)
          this.state = {
              loginButton: false,
              userValue : {}
          }
         
    }

    componentWillMount(){
            let userInfo = localStorage.getItem('userInfo');
            console.log("this is user button ",userInfo);
            if(userInfo == null){
                  this.setState({
                      loginButton:true
                  })
            }else{
                 this.setState({
                     loginButton:false
                 })
            }
    }
    render() {
        const {loginButton} =this.state
        return (
        
       <div className="container">
           <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <Link to={'/'} className="navbar-brand">React CRUD Example</Link>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                { loginButton === true ?(
                        <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link to={'/signIn'} className="nav-link">SignIn</Link>
                          </li>
                          <li className="nav-item">
                            <Link to={'/signUp'} className="nav-link">SignUp</Link>
                          </li>
                          
                        </ul>
                ):(
                    <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link to={'/'} className="nav-link">Home</Link>
                      </li>
                      <li className="nav-item">
                        <Link to={'/create'} className="nav-link">Create</Link>
                      </li>
                      <li className="nav-item">
                        <Link to={'/edit/:id'} className="nav-link">Edit</Link>
                      </li>
                    </ul>
                )
            //   <ul className="navbar-nav mr-auto">
            //   <li className="nav-item">
            //       <Link to={'/'} className="nav-link">Home</Link>
            //     </li>
            //     <li className="nav-item">
            //       <Link to={'/create'} className="nav-link">Create</Link>
            //     </li>
            //     <li className="nav-item">
            //       <Link to={'/edit/:id'} className="nav-link">Edit</Link>
            //     </li>
            //   </ul>
                }
            </div>
          </nav> <br/>
          <h2>Welcome to React CRUD Tutorial</h2> <br/>
            </div>
        )
    }
}

