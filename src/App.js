import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import React from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Create from '../src/component/create/index.js';
import Edit from '../src/component/edit/index.js';
import Main from '../src/component/main/index.js';
import Header from '../src/component/header/header.js';
import signIn from '../src/component/SignIn/index.js';
import signUp from '../src/component/SignUp/index.js';




function App() {
      
  

  return (
    <Router>
      <div>
          <Header/>
          <div className="container">
          <Switch>
              <Route exact path='/' component={ Main } />
              <Route path='/signIn' component={ signIn } />
              <Route path='/signUp' component={ signUp } />
              <Route path='/edit/:id' component={ Edit } />
              <Route path='/create' component={ Create } />

          </Switch>
          </div>
      </div>
    </Router>
  );
}

export default App;